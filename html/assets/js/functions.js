"use strict";

var $window = $(window);
var $bodyHtml = $('body,html');
var breakPoint1 = 767;
/* ======================================
plus
====================================== */

$(".js-plus").on("click", function (event) {
  event.preventDefault();
  if ($(this).parent().next().is(':animated')) return;
  $(this).toggleClass('is-active');

  if ($(this).hasClass('is-active')) {
    $(this).parent().next().slideDown(500, function () {
      var $footer_obj = $('.iframe-footer', parent.document),
          footer_height = $footer_obj.height(),
          acd_height = $(this).parent().find('.c-footer__nav2').height();
      $footer_obj.css({
        'height': footer_height + acd_height,
        'max-height': footer_height + acd_height
      });
    });
  } else {
    $(this).parent().next().slideUp(500, function () {
      var $footer_obj = $('.iframe-footer', parent.document),
          footer_height = $footer_obj.height(),
          acd_height = $(this).parent().find('.c-footer__nav2').height();
      $footer_obj.css({
        'height': footer_height - acd_height,
        'max-height': footer_height - acd_height
      });
    });
  }
});
/* ======================================
top page
====================================== */

$(".c-top p").on("click", function () {
  parent.$bodyHtml.animate({
    scrollTop: 0
  }, 700);
  return false;
});
/* ======================================
scroll to id
====================================== */

$(".js-anchor a").on("click", function () {
  var toId = $(this).attr('href');

  if ($window.width() > 767) {
    $bodyHtml.animate({
      scrollTop: $(toId).offset().top
    }, 700);
  } else {
    $bodyHtml.animate({
      scrollTop: $(toId).offset().top - 60
    }, 700);
  }

  return false;
});
/* ======================================
c-slide1
====================================== */

$(".c-slide1").slick({
  dots: true,
  speed: 1000,
  autoplaySpeed: 3500,
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  arrows: true,
  nextArrow: '<span class="next"></span>',
  prevArrow: '<span class="pre"></span>',
  focusOnSelect: false,
  pauseOnHover: false,
  accessibility: false,
  lazyLoad: 'ondemand'
});
/* ======================================
iframe �⤵Ĵ��
====================================== */

$("iframe").on("load", function () {
  try {
    $(this).height(0);
    $(this).height(this.contentWindow.document.documentElement.scrollHeight);
  } catch (e) {}
});
$("iframe").trigger("load");
/* ======================================

====================================== */

$('.c-header__iconSP').on('click', function () {
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    parent.document.menuFrame.document.getElementsByClassName('c-test')[0].classList.remove('open');
    parent.document.menuFrame.document.getElementsByClassName('u-overlay')[0].classList.remove('open');
    parent.document.getElementsByClassName('iframe-menu')[0].classList.remove('open');
  } else {
    $(this).addClass('active');
    parent.document.menuFrame.document.getElementsByClassName('c-test')[0].classList.add('open');
    parent.document.menuFrame.document.getElementsByClassName('u-overlay')[0].classList.add('open');
    parent.document.getElementsByClassName('iframe-menu')[0].classList.add('open');
    parent.document.getElementsByTagName('html')[0].classList.add('scroll-test');
  }
});
$('.u-overlay').on('click', function () {
  var _this = this;

  if ($(this).hasClass('open')) {
    parent.document.headerFrame.document.getElementsByClassName('c-header__iconSP')[0].classList.remove('active');
    parent.document.menuFrame.document.getElementsByClassName('c-test')[0].classList.remove('open');
    parent.document.getElementsByTagName('html')[0].classList.remove('scroll-test');
    setTimeout(function () {
      $(_this).removeClass('open');
      parent.document.getElementsByClassName('iframe-menu')[0].classList.remove('open');
    }, 500);
  }
});
var ua = window.navigator.userAgent.toLowerCase();

if (ua.indexOf('iPhone') > 0 && ua.indexOf('safari') !== -1 && ua.indexOf('chrome') === -1 && ua.indexOf('edge') === -1) {
  window.onpageshow = function (event) {
    if (event.persisted) {
      window.location.reload();
    }
  };
}