/**
 * gulp??��??�????
 */
module.exports.setting = {
	html: {
		src: 'dev/ejs/',
		dest: 'html/',
		meta: 'dev/ejs/_inc/meta.json',
		validate: true
	},
	css: {
		minify: false,
		map: false,
		src: 'dev/scss/',
		dest: 'html/assets/css/'
	},
	js: {
		webpack: false,
		minify: false,
		babel: true,
		src: 'dev/js/',
		dest: 'html/assets/js/'
	},
	imagemin: {
		path: 'html/assets/img/',
		quality: '80-90'
	},
	server: {
		base: 'html',
		watch: './html'
	}
};

/**
 * ??��?��????��?��?��?��?????設�??
 */
module.exports.loadPlugins = {
	pattern: [
		'gulp-*',
		'gulp.*',
		'browser-sync',
		'run-sequence',
		'imagemin-pngquant',
		'imagemin-jpegtran',
		'node-sass-package-importer'
	],
	rename: {
		'browser-sync': 'browserSync',
		'run-sequence': 'sequence',
		'imagemin-pngquant': 'pngquant',
		'imagemin-jpegtran': 'jpegtran',
		'gulp-connect-php': 'php',
		'gulp-clean-css': 'cleanCSS',
		'node-sass-package-importer': 'sassImporter'
	}
};
